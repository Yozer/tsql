﻿use Pegasus
GO

--insert into Users(Username) values (dbo.CreateRandomString(15))
--GO 1000
--insert into Users(Username) values ('PEGASUS_PROD')
--GO

--insert into dbo.ProductSubCategory(Name) values (dbo.CreateRandomString(50))
--GO 100

--;with countries_ctes as
--(
--select 'United Kingdom' as Location union
--select 'Turkmenistan' as Location union
--select 'Sweden' as Location union
--select 'Slovakia' as Location union
--select 'Venezuela' as Location union
--select 'Saint Vincent & the Grenadines' as Location union
--select 'Guatemala' as Location union
--select 'Djibouti' as Location union
--select 'Greece' as Location union
--select 'India' as Location union
--select 'Uzbekistan' as Location union
--select 'Bolivia' as Location union
--select 'Niger' as Location union
--select 'Armenia' as Location union
--select 'United Arab Emirates' as Location union
--select 'Afghanistan'
--),
--legal_entity_cte as 
--(
--select 'Taloqan' as LegalEntity union
--select 'Balkh' as LegalEntity union
--select 'Gereshk' as LegalEntity union
--select 'Burrel' as LegalEntity union
--select 'Āsmār' as LegalEntity union
--select 'Barakī Barak' as LegalEntity union
--select 'Sar-e Pul' as LegalEntity union
--select 'Shibirghān' as LegalEntity union
--select 'Ārt Khwājah' as LegalEntity union
--select 'Lushnjë' as LegalEntity union
--select 'Andkhōy' as LegalEntity
--),
--business_cte as
--(
--select 'moved' as Business union
--select 'property' as Business union
--select 'fit' as Business union
--select 'enemy' as Business union
--select 'states' as Business
--)

--insert into Portfolio(Name, Location, LegalEntity, Business)
--select top 100 dbo.CreateRandomString(45) as Name, a.Location, b.LegalEntity, c.Business from countries_ctes a, legal_entity_cte b, business_cte c
--order by NEWID()
--GO

--with dates_cte as
--(
--	select CAST('2019-04-30' as DATE) as AsOfDate
--	union all
--	select dbo.GetLastBusinessDay(dateadd(month, -1, AsOfDate)) as AsOfDate
--	from dates_cte
--	where AsOfDate > '2016-09-01'
--)
--insert into DataManagerHeader(AsOfDate)
--select AsOfDate from dates_cte
--GO


--;with dates_cte as
--(
--	select CAST('2019-04-30' as DATE) as AsOfDate
--	union all
--	select dateadd(day, -1, AsOfDate) as AsOfDate
--	from dates_cte
--	where AsOfDate > '2015-01-01'
--)
--insert into fx_run(AsOfDate)
--select AsOfDate from dates_cte
--option (maxrecursion 0)
--GO

--;with currency as
--(
--select 'GGP' as Currency union
--select 'YER' as Currency union
--select 'IMP' as Currency union
--select 'VND' as Currency union
--select 'GYD' as Currency union
--select 'MNT' as Currency union
--select 'PLN' as Currency union
--select 'UAH' as Currency union
--select 'MKD' as Currency union
--select 'AUD' as Currency union
--select 'FJD' as Currency union
--select 'HUF' as Currency union
--select 'CRC' as Currency union
--select 'UYU' as Currency union
--select 'CHF' as Currency union
--select 'EGP' as Currency union
--select 'KPW' as Currency union
--select 'TRY' as Currency union
--select 'BZD' as Currency union
--select 'HNL' as Currency union
--select 'BRL' as Currency union
--select 'MXN' as Currency union
--select 'NOK' as Currency union
--select 'GTQ' as Currency union
--select 'CZK' as Currency union
--select 'BWP' as Currency union
--select 'DKK' as Currency union
--select 'TRL' as Currency union
--select 'GHC' as Currency union
--select 'INR' as Currency union
--select 'BBD' as Currency union
--select 'NPR' as Currency union
--select 'MYR' as Currency union
--select 'GBP' as Currency union
--select 'SGD' as Currency union
--select 'USD' as Currency union
--select 'GIP' as Currency union
--select 'BOB' as Currency union
--select 'AFN' as Currency union
--select 'FKP' as Currency union
--select 'PEN' as Currency union
--select 'RSD' as Currency union
--select 'LTL' as Currency union
--select 'KZT' as Currency union
--select 'OMR' as Currency union
--select 'DOP' as Currency union
--select 'SCR' as Currency union
--select 'THB' as Currency
--)
--insert into fx_details(RunId, FromCCY, Rate)
--select run.Id as RunId, curr.Currency as FromCCy, (select CONVERT( DECIMAL(18, 12), 0.001 + (12-0.001)*RAND(CHECKSUM(NEWID())))) as Rate
--from fx_run run, currency curr
--GO

--;with currency as
--(
--select 'GGP' as Currency union
--select 'YER' as Currency union
--select 'IMP' as Currency union
--select 'VND' as Currency union
--select 'GYD' as Currency union
--select 'MNT' as Currency union
--select 'PLN' as Currency union
--select 'UAH' as Currency union
--select 'MKD' as Currency union
--select 'AUD' as Currency union
--select 'FJD' as Currency union
--select 'HUF' as Currency union
--select 'CRC' as Currency union
--select 'UYU' as Currency union
--select 'CHF' as Currency union
--select 'EGP' as Currency union
--select 'KPW' as Currency union
--select 'TRY' as Currency union
--select 'BZD' as Currency union
--select 'HNL' as Currency union
--select 'BRL' as Currency union
--select 'MXN' as Currency union
--select 'NOK' as Currency union
--select 'GTQ' as Currency union
--select 'CZK' as Currency union
--select 'BWP' as Currency union
--select 'DKK' as Currency union
--select 'TRL' as Currency union
--select 'GHC' as Currency union
--select 'INR' as Currency union
--select 'BBD' as Currency union
--select 'NPR' as Currency union
--select 'MYR' as Currency union
--select 'GBP' as Currency union
--select 'SGD' as Currency union
--select 'USD' as Currency union
--select 'GIP' as Currency union
--select 'BOB' as Currency union
--select 'AFN' as Currency union
--select 'FKP' as Currency union
--select 'PEN' as Currency union
--select 'RSD' as Currency union
--select 'LTL' as Currency union
--select 'KZT' as Currency union
--select 'OMR' as Currency union
--select 'DOP' as Currency union
--select 'SCR' as Currency union
--select 'THB' as Currency
--)
--insert into fx_details(RunId, FromCCY, Rate)
--select run.Id as RunId, curr.Currency as FromCCy, (select CONVERT( DECIMAL(18, 12), 0.001 + (12-0.001)*RAND(CHECKSUM(NEWID())))) as Rate
--from fx_run run, currency curr
--GO

insert into ReviewerAccess(LocationId, LegalEntity, BusinessId, Username)
select distinct LocationId, LegalEntityId, BusinessNameId, 'PEGASUS_PROD' as Username from Portfolio
GO


declare @user varchar(100) = (select top 1 Username from Users order by NEWID())
insert into ReviewerAccess(LocationId, LegalEntity, BusinessId, Username)
select distinct LocationId, LegalEntityId, BusinessNameId, @user as Username from Portfolio
GO 500