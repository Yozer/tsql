use pegasus
go
SET STATISTICS IO, TIME ON;
go

declare @username varchar(100) = 'PEGASUS_PROD';
declare @asOfDate date = '2019-02-28';
declare @previousDate date = dbo.GetLastBusinessDay(dateadd(month, -1, @asOfDate));


;with aggregated_uploads_cte as
(
	select portfolio.LocationId, portfolio.LegalEntityId, portfolio.BusinessNameId, fva.ProductSubCategoryId, header.AsOfDate,
		   SUM(fva.Balance * fx.Rate) as [MonthBalance]
	from dbo.FvaData fva
	inner join DataManagerHeader header on header.HdrId = fva.HeaderId
	inner join Portfolio portfolio on portfolio.Id = fva.PortfolioId
	outer apply dbo.GetFxRate(header.AsOfDate, fva.Currency) fx
	group by portfolio.LocationId, portfolio.LegalEntityId, portfolio.BusinessNameId, fva.ProductSubCategoryId, header.AsOfDate
	having header.AsOfDate in (@asOfDate, @previousDate) and exists (select * from ReviewerAccess access
										where access.Username = @username and access.BusinessId = portfolio.BusinessNameId and access.LegalEntity = portfolio.LegalEntityId and access.LocationId = portfolio.LocationId)
),
uploads_cte as
(
	select loc.Name as Location, b.Name as Business, l.Name as LegalEntity, upload.ProductSubCategoryId, upload.AsOfDate, upload.MonthBalance,
			LAG(upload.MonthBalance, 1, 0) over (partition by upload.LocationId, upload.LegalEntityId, upload.BusinessNameId, upload.ProductSubCategoryId order by upload.AsOfDate) as [PriorMonthBalance]
	from aggregated_uploads_cte upload
	inner join LocationName loc on loc.Id = upload.LocationId
	inner join BusinessName b on b.Id = upload.BusinessNameId
	inner join LegalEntity l on l.Id = upload.LegalEntityId
)

select * from uploads_cte where asofdate = @asOfDate
order by Location, LegalEntity, Business, ProductSubCategoryId