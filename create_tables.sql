use Pegasus
Go

create table ProductSubCategory
(
	Id int identity(1,1) primary key,
	Name varchar(200)
)
GO

create table Portfolio
(
	Id int identity(1,1) primary key,
	Name varchar(200),
	Location varchar(200),
	LegalEntity varchar(200),
	Business varchar(200)
)
GO

create table DataManagerHeader
(
	HdrId int identity(1, 1) primary key,
	AsOfDate Date not null
)
GO

create table FvaData
(
	Id int identity(1,1) primary key,
	HeaderId int not null foreign key references DataMAnagerHeader(HdrId),
	Balance decimal(32, 12) not null,
	Currency varchar(3) not null,
	ProductSubCategoryId int not null foreign key references ProductSubCategory(Id),
	PortfolioId int not null foreign key references Portfolio(Id),
	UploadGuid uniqueidentifier default NEWSEQUENTIALID()
)
GO

create table Users
(
	Username varchar(200) primary key
)
GO

create table ReviewerAccess
(
	LocationId int not null references LocationName(Id),
	BusinessId int not null references BusinessName(Id),
	LegalEntity int not null references LegalEntity(Id),
	Username varchar(200) not null foreign key references Users(Username)
)
GO

create table SignOffs
(
	UploadGuid uniqueidentifier not null,
	LegalEntity varchar(200),
	Business varchar(200),
	ProductSubCategoryId int not null foreign key references ProductSubCategory(Id),
	Approver varchar(200) not null foreign key references Users(Username),
	ApprovedTime date not null default GETUTCDATE(),
	Status varchar(25) not null,
	Comment varchar(1024) not null
)
GO

create table fx_run
(
	Id int identity(1, 1),
	AsOfDate Date not null
)
GO

create table fx_details
(
    RunId int not null foreign key references fx_run(Id),
	Rate decimal(32, 12) not null,
	FromCCY varchar(3) not null,
)
GO