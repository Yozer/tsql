use Pegasus
GO

CREATE OR ALTER FUNCTION dbo.GetLastBusinessDay(@asOfDate Date)  
RETURNS Date
AS
BEGIN   
	declare @date as date= eomonth(@asOfDate,0)
	set @date = (select case
		when datepart(dw,@date) = 1 then dateadd(day,-2,@date) --when day is a sunday subtract 2 day to friday
		when datepart(dw,@date) = 7 then dateadd(day,-1,@date) --when day is a saturday subtract 1 day to friday
		else @date END)
	return @date
END 
GO

CREATE OR ALTER FUNCTION dbo.GetFxRate(@asOfDate Date, @currency varchar(3))  
RETURNS TABLE
AS
RETURN   
select top 1 details.Rate from dbo.fx_run run
		inner join dbo.fx_details details on run.Id = details.RunId
		where run.AsOfDate between dateadd(day, -5, @asOfDate) and @asOfDate
		order by run.AsOfDate desc

GO


create or alter view [dbo].[RandHelper]
as select rand( ) as r
GO

CREATE or alter function [dbo].[CreateRandomString] (
 @passwordLength as smallint
)
RETURNS varchar(100)

AS

begin
	declare @password varchar(100)
	declare @characters varchar(100)
	declare @count int

	set @characters = ''

	 -- load up numbers 0 - 9
	 set @count = 48
	 while @count <=57
	 begin
	     set @characters = @characters + Cast(CHAR(@count) as char(1))
	     set @count = @count + 1
	 end

	 -- load up uppercase letters A - Z
	 set @count = 65
	 while @count <=90
	 begin
	     set @characters = @characters + Cast(CHAR(@count) as char(1))
	     set @count = @count + 1
	 end

	 -- load up lowercase letters a - z
	 set @count = 97
	 while @count <=122
	 begin
	     set @characters = @characters + Cast(CHAR(@count) as char(1))
	     set @count = @count + 1
	 end


	set @count = 0
	set @password = ''


	 while @count < @passwordLength
	 begin
	     set @password = @password + SUBSTRING(@characters,CAST(CEILING((SELECT r FROM RandHelper)*LEN(@characters)) as int),1)
	     set @count = @count + 1
	 end

	RETURN @password
end