use pegasus
go
SET STATISTICS IO, TIME ON;
go

declare @username varchar(100) = 'PEGASUS_PROD';
declare @asOfDate date = '2019-02-28';
declare @previousDate date = dbo.GetLastBusinessDay(dateadd(month, -1, @asOfDate));


;with aggregated_uploads_cte as
(
	select portfolio.Location, portfolio.LegalEntity, portfolio.Business, fva.ProductSubCategoryId, header.AsOfDate,
		   SUM(fva.Balance * fx.Rate) as [MonthBalance]
	from dbo.FvaData fva
	inner join DataManagerHeader header on header.HdrId = fva.HeaderId
	inner join Portfolio portfolio on portfolio.Id = fva.PortfolioId
	outer apply dbo.GetFxRate(header.AsOfDate, fva.Currency) fx
	group by portfolio.Location, portfolio.LegalEntity, portfolio.Business, fva.ProductSubCategoryId, header.AsOfDate
	having header.AsOfDate in (@asOfDate, @previousDate) and exists (select * from ReviewerAccess access
																	 where access.Username = @username and access.Business = portfolio.Business and access.LegalEntity = portfolio.LegalEntity and access.Location = portfolio.Location)
),
uploads_cte as
(
	select upload.*,
			LAG(upload.MonthBalance, 1, 0) over (partition by upload.Location, upload.LegalEntity, upload.Business, upload.ProductSubCategoryId order by upload.AsOfDate) as [PriorMonthBalance]
	from aggregated_uploads_cte upload
)

select * from uploads_cte where asofdate = @asOfDate
order by Location, LegalEntity, Business, ProductSubCategoryId