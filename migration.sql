use pegasus
go

create table LocationName
(
	[Id] int identity(1,1) primary key,
	[Name] varchar(200) UNIQUE not null
)
GO

create table BusinessName
(
	[Id] int identity(1,1) primary key,
	[Name] varchar(200) UNIQUE not null
)
GO

create table LegalEntity
(
	[Id] int identity(1,1) primary key,
	[Name] varchar(200) UNIQUE not null
)
GO

insert into LocationName([Name])
select distinct [Location] from Portfolio
GO

insert into BusinessName([Name])
select distinct [Business] from Portfolio
GO

insert into LegalEntity([Name])
select distinct [LegalEntity] from Portfolio
GO

ALTER TABLE Portfolio ADD
LocationId int references LocationName(Id),
BusinessNameId int references BusinessName(Id),
LegalEntityId int references LegalEntity(Id)
GO


update portfolio 
set portfolio.LocationId = location.Id, portfolio.BusinessNameId = business.Id, portfolio.LegalEntityId = legal.Id
FROM dbo.Portfolio AS portfolio
inner join LocationName location on location.Name = portfolio.Location
inner join BusinessName business on business.Name = portfolio.Business
inner join LegalEntity legal on legal.Name = portfolio.LegalEntity
GO

ALTER TABLE Portfolio ALTER COLUMN LocationId int not null
GO
ALTER TABLE Portfolio ALTER COLUMN BusinessNameId int not null
GO
ALTER TABLE Portfolio ALTER COLUMN LegalEntityId int not null
GO

ALTER TABLE Portfolio DROP COLUMN Location, Business, LegalEntity
GO