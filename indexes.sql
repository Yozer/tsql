CREATE NONCLUSTERED INDEX [_dta_index_FvaData_5_1797581442__K2_K6_K5_3_4] ON [dbo].[FvaData]
(
	[HeaderId] ASC,
	[PortfolioId] ASC,
	[ProductSubCategoryId] ASC
)
INCLUDE ( 	[Balance], [Currency]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
Go
CREATE NONCLUSTERED INDEX [_dta_index_FvaData_5_1797581442__K2_K6_K5_3_4]
ON [dbo].[FvaData] ([PortfolioId])
INCLUDE ([HeaderId],[Balance],[ProductSubCategoryId])


CREATE NONCLUSTERED INDEX [_dta_index_Portfolio_5_1765581328__K5_K4_K3] ON [dbo].[Portfolio]
(
	[Business] ASC,
	[LegalEntity] ASC,
	[Location] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
Go

-- after migration
CREATE NONCLUSTERED INDEX [_dta_index_Portfolio_5_322100188__K7_K8_K6_K1] ON [dbo].[Portfolio]
(
	[BusinessNameId] ASC,
	[LegalEntityId] ASC,
	[LocationId] ASC,
	[Id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


CREATE unique clustered INDEX [_dta_index_ReviewerAccess_5_1125579048__K5_K3_K2_K1] ON [dbo].[ReviewerAccess]
(
	[Username] ASC,
	[Business] ASC,
	[LegalEntity] ASC,
	[Location] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

-- after migration
CREATE unique clustered INDEX [_dta_index_ReviewerAccess_5_1125579048__K5_K3_K2_K1] ON [dbo].[ReviewerAccess]
(
	[Username] ASC,
	[BusinessId] ASC,
	[LegalEntity] ASC,
	[LocationId] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE unique CLUSTERED INDEX [_dta_index_fx_details_c_5_1925581898__K1] ON [dbo].[fx_details]
(
	[RunId] ASC,
	[FromCCY]
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX IX_TestTable_TestCol1   
    ON dbo.fx_run (asofdate) include(id);   