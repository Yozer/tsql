use pegasus
go


IF OBJECT_ID('tempdb..#currencies') IS NOT NULL DROP Table #currencies
IF OBJECT_ID('tempdb..#portfolios') IS NOT NULL DROP Table #portfolios
IF OBJECT_ID('tempdb..#subCategory') IS NOT NULL DROP Table #subCategory
;with currency as
(
select 'GGP' as Currency union
select 'YER' as Currency union
select 'IMP' as Currency union
select 'VND' as Currency union
select 'GYD' as Currency union
select 'MNT' as Currency union
select 'PLN' as Currency union
select 'UAH' as Currency union
select 'MKD' as Currency union
select 'AUD' as Currency union
select 'FJD' as Currency union
select 'HUF' as Currency union
select 'CRC' as Currency union
select 'UYU' as Currency union
select 'CHF' as Currency union
select 'EGP' as Currency union
select 'KPW' as Currency union
select 'TRY' as Currency union
select 'BZD' as Currency union
select 'HNL' as Currency union
select 'BRL' as Currency union
select 'MXN' as Currency union
select 'NOK' as Currency union
select 'GTQ' as Currency union
select 'CZK' as Currency union
select 'BWP' as Currency union
select 'DKK' as Currency union
select 'TRL' as Currency union
select 'GHC' as Currency union
select 'INR' as Currency union
select 'BBD' as Currency union
select 'NPR' as Currency union
select 'MYR' as Currency union
select 'GBP' as Currency union
select 'SGD' as Currency union
select 'USD' as Currency union
select 'GIP' as Currency union
select 'BOB' as Currency union
select 'AFN' as Currency union
select 'FKP' as Currency union
select 'PEN' as Currency union
select 'RSD' as Currency union
select 'LTL' as Currency union
select 'KZT' as Currency union
select 'OMR' as Currency union
select 'DOP' as Currency union
select 'SCR' as Currency union
select 'THB' as Currency
),

currency_with_usd as
(
select * from currency UNION
select 'USD' as Currency
)

SELECT top 30 * 
INTO #currencies
from currency_with_usd
order by NEWID()
GO

SELECT top 20 * 
INTO #portfolios
from Portfolio
order by NEWID()
GO

SELECT * 
INTO #subCategory
from ProductSubCategory
order by NEWID()
GO


insert into FvaData(HeaderId, Balance, Currency, ProductSubCategoryId, PortfolioId)
select top 250000 hdr.HdrId as HeaderId, 
	(select CONVERT( DECIMAL(32, 12), ABS(CHECKSUM(NEWID()) % 10000000))) as [Balance],
	curr.Currency,
	subCat.Id as ProductSubCategoryId,
	port.Id as PortfolioId
from DataManagerHeader hdr, #currencies curr, #subCategory subCat, #portfolios port
ORDER BY NEWID()

GO
